import {ApolloServer, gql} from 'apollo-server-express';
// import {getSpecificDoc} from "../../database/getSpecificDoc";
import {getCollectionData} from "../../database/getCollectionData";
import {loggerApolloPlugin} from "../../logger/apolloIntegration";

const apiSchema = gql`
	type Query{
		hello: String
		testMessages: [Message]
	}
	
	type Message{
		id: String
		message: String
		addedOn: String
	}
`

const apiResolvers = {
	Query: {
		hello: ()=> {
			return "Hello World!"
		},
		testMessages: async ()=>{
			const docs = await getCollectionData("test_database","test")
			return docs.map(e=> {
				return {
					id: e._id,
					message: e.message,
					addedOn: e.addedOn
				}
			})
		}
	}
}

export const GQLServer = new ApolloServer({typeDefs: apiSchema, resolvers:apiResolvers, plugins:[
	loggerApolloPlugin
]})