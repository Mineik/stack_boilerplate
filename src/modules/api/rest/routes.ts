import {Router, json} from "express"
import {logger} from "../../logger/logger";
import {getCollectionData} from "../../database/getCollectionData";
import {getSpecificDoc} from "../../database/getSpecificDoc";
import {loggerMiddleware} from "../../logger/expressMiddleware";
import cors from "cors";


const router = Router()
router.use(json())
router.use(cors())


router.get('/ping', (req,res, next)=>{
	res.status(200)
	res.json({
		message: "hello world!"
	})
	next()
})

router.get('/testTableData', async (req, res, next)=>{
	try{
		res.json(await getCollectionData("test_database","test"))
		res.status(200)
	}catch(e){
		logger(2, e)
		res.status(503)
		res.jsonp(e)
	}
	next()
})

router.get("/testMessage", async (req, res, next)=>{
	try{
		const doc = await getSpecificDoc("test_database","test",{addedOn: "2020-10-15"})
		res.status(200).jsonp(doc)
	}catch (e){
		res.status(503).json(e)
	}
	next()
})


router.get("/message", async (req,res,next)=>{
	res.status(501)
	res.jsonp({message: "Not implemented"})
	next()
})


router.use("/**", (req,res, next)=>{
	if(!res.headersSent){
		res.status(404)
		res.json({
			message: "Not Found"
		})
	}
	next()
})
router.use(loggerMiddleware)
export {router as apiRouter}
