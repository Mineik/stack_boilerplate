import {client} from "./index";

// eslint-disable-next-line @typescript-eslint/ban-types
export async function addDocToCollection(db: string, collection: string, data: object){
	return await client.db(db).collection(collection).insertOne(data)
}