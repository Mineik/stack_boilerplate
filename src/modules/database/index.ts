import {MongoClient} from "mongodb";

const url = "mongodb://dan:toor@mongo"

export const client = new MongoClient(url, {
	useUnifiedTopology: true,
})
export async function connectToClient(){
	await client.connect() //we only connect once on application start
}

export async function disconnectFromClient(){
	await client.close()
}
export {MongoClient};