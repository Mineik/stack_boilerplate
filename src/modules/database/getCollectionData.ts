import {client} from "./index"

export async function getCollectionData(db:string, collection: string){
	const data = await client.db(db).collection(collection).find().toArray()
	return data
}