import {client} from "./index";

export async function getSpecificDoc(db, collection, query){
	const doc = await client.db(db).collection(collection).findOne(query)
	return doc
}