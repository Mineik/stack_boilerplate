import chalk from "chalk"
import dotenv from "dotenv"
import {Request} from "express"
dotenv.config()

export enum logTypes{
	INFO,
	DEBUG,
	ERROR,
	WARING,
	HTTP,
	GQL
}

function log(type: logTypes, message){
	const date = new Date()
	let outputString = `[${chalk.gray(date.toISOString())}]`
	switch(type){
	case logTypes.INFO:
		outputString+=`${chalk.blue(" INFO ")}`
		break
	case logTypes.DEBUG:
		if(process.env.MODE != "debug") return //quit function immidiately
		else{
			outputString += chalk.green(" DEBUG ")
		}
		break
	case logTypes.ERROR:
		outputString+= chalk.red(" ERR ")
		break
	case logTypes.WARING:
		outputString += chalk.yellow(" WARN ")
		break
	case logTypes.HTTP:
		outputString += chalk.cyan(" HTTP ")
		break
	case logTypes.GQL:
		outputString+=chalk.magentaBright(" GQL ")
		break
		
	default:
		log(logTypes.ERROR, "Attempt to log unknown type, logging as nonetype")
		outputString += chalk.black(" NONE ")
		break
	}

	outputString+=chalk.white(message)
	console.log(outputString)
}


export function logHTTPReq(req: Request, status: number){
	log(logTypes.HTTP, `${req.method} ${req.baseUrl}${req.path} -> ${status}`)
}

export function logGQLReq(success: boolean){
	log(logTypes.GQL, `/graphql -> ${success? "Success":"Failure"}`)
}

export function logger(type: logTypes|number=logTypes.INFO, message: string){
	log(type, message)
}