import {logHTTPReq} from "./logger";

export function loggerMiddleware(req,res, next){
	logHTTPReq(req, res.statusCode)
	if(!req.baseUrl.startsWith("/")) next()
}