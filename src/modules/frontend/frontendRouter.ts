import {Router, static as staticMiddle} from "express"
import {join} from "path"
import webpack from "webpack"
import WebpackConf from "../../../webpack.config"
import {logger} from "../logger/logger";
// @ts-ignore
webpack(WebpackConf).run((err, stat)=>{
	logger(stat.hasErrors()?2:0, `Frontend packed with${stat.hasErrors()?"":"out"} errors`)
	logger(stat.hasWarnings()?3:0, `Frontend packed with${stat.hasWarnings()?"":"out"} warnings`)
	if(err){
		logger(2, JSON.stringify(err))
	}
})

const frontendRouter = Router()
frontendRouter.use(staticMiddle(join(__dirname, "../../../static"), {
	etag: false,
	extensions: ['html, htm'],
	fallthrough: true
}))

frontendRouter.use((req,res,next)=>{
	res.redirect(req.baseUrl+req.path.replace(/\//,"/#/"))
	next()
})

export { frontendRouter }