//eslint-disable-next-line no-undef
module.exports= {
	root: false,
	parser: "vue-eslint-parser",
	plugins: [
		"vue"
	],
	extends: [
		"plugin:vue/recommended",
	],
	rules: {
		"vue/html-indent": [2, "tab"],
		"vue/script-indent": [2, "tab"],
		"@typescript-eslint/indent": [2, "tab"],
		"no-console": 0,
		"vue/match-component-file-name": [2, {
			extensions: ["vue"],
			shouldMatchCase: false
		}]
	}
}
