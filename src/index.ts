import {logger, logTypes} from "./modules/logger/logger";
import {apiRouter} from "./modules/api/rest/routes"
import {GQLServer} from "./modules/api/graphql/apollo"
import {connectToClient, disconnectFromClient} from "./modules/database";
import {loggerMiddleware} from "./modules/logger/expressMiddleware";
import {frontendRouter} from "./modules/frontend/frontendRouter";

logger(0, "Initializing server...")
import express from "express";
import cors from "cors";

connectToClient().then(()=>{
	logger(0, "Connected to mongoDB client")
})


const app = express()
app.use(express.json())
app.use(cors())
app.use('/api', apiRouter)
GQLServer.applyMiddleware({app})
app.use("/", frontendRouter)
app.use(loggerMiddleware)
app.disable("x-powered-by")

app.listen({port:3000},()=>{
	logger(0,"Up and running")
})

process.on("beforeExit",async ()=>{
	await disconnectFromClient().then(()=>{
		logger(0, "Disconnected from database")
	})
})

process.on("exit", (code)=>{
	logger(code===0? logTypes.INFO:logTypes.ERROR,`Exited witch code ${code}`)
})