const HTMLWebpackPlugin = require("html-webpack-plugin")
const { VueLoaderPlugin } = require("vue-loader")
const { join } = require("path")

module.exports = {
	mode: 'development',
	entry: join(__dirname, "src/modules/frontend/app/appRenderer.ts"),
	output: {
		path: join(__dirname, "static")

	},
	module: {
		rules: [
			{
				test: /\.(ts|js)$/,
				loader: "babel-loader"
			},
			{
				test: /\.vue$/,
				loader: "vue-loader"
			},
			{
				test: /\.css$/,
				use: [
					"vue-style-loader",
					"css-loader"
				]
			}
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new HTMLWebpackPlugin({
			showErrors: true,
			cache: true,
			template: join(__dirname, "src/modules/frontend/appEntrypoint.html"),
			title: "Webpack App"
		})
	]
}
